#include "NVREventHandler.h"
#include <iostream>

using namespace E40::NVRSDK;
using namespace std;

NVREventHandler::NVREventHandler(int t_num)
{
	m_num = t_num; 
}

void NVREventHandler::HandleEvent(NVREventMsg eventMsg)
{

	if (eventMsg.eventId == 0 && eventMsg.channel >= 0 && eventMsg.channel <= 32)
	{
		std::cout << "channel(" << eventMsg.channel << ") online stauts:" << eventMsg.status << std::endl;
	}

	if (eventMsg.eventId == 1 && eventMsg.channel >= 0 && eventMsg.channel <= 32)
	{
		std::cout << "chanal(" << eventMsg.channel << ") recording stauts:" << eventMsg.status << std::endl;
	}

	if (eventMsg.eventId == 2 && eventMsg.channel >= 0 && eventMsg.channel <= 32)
	{
		std::cout << "chanal(" << eventMsg.channel << ") motion detected:" << eventMsg.status << " field:" << eventMsg.extraData << std::endl;
	}


	if (eventMsg.eventId == 3 && eventMsg.channel == -1)
	{
		std::cout << "SYS_TECHNICIAN_GRANT stauts:" << eventMsg.status << std::endl;
	}
}

