#ifndef NVRSDK_NVREVENTHANDLER
#define NVRSDK_NVREVENTHANDLER
#include "INVRDevice.h"
namespace E40 {
	namespace NVRSDK {
		class NVREventHandler:public INVREventHandler
		{
		public:
			NVREventHandler(int t_num);
			virtual void HandleEvent(NVREventMsg eventMsg);
		private:
			int m_num = 0;
		};
	}
}
#endif // NVRSDK_NVREVENTHANDLER
