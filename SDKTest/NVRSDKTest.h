#pragma once

#include <string>
#include "NVRPlayback.h"
#include"NVREventHandler.h"

class NVRSDKTest
{
public:
	NVRSDKTest();
    NVRSDKTest(std::string ip, int port, std::string user, std::string pwd);
	~NVRSDKTest();
	void TestLogin();
	void TestGetVersion();
	void TestGetCameraList();
	void TestGetStatus();
	void TestGetUrl();
	void TestGetRecord();


    void TestSetLayout();


private:
	static void PlaybackCallback(E40::NVRSDK::DeviceRecordInfo pRecordInfo);

private:
	std::string _deviceIP;
	std::string _deviceUser;
	std::string _devicePassword;
	unsigned short _devicePort;
	E40::NVRSDK::NVREventHandler *m_event;
};

