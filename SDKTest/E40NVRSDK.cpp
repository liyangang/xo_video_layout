// E40NVRSDK.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include "pch.h"
#include <iostream>
#include "NVRSDKTest.h"

using namespace std;
int main(int argc, char * argv[])
{
    cout << "************************************" << endl;
    cout << "*  Welcome to NVR SDK Test.  *" << endl;
    cout << "************************************" << endl;
    cout << endl;
    if (argc != 5)
    {
        cout << "Usage: program_name ip port user_name user_password " << endl;
       // return -1;
    }
	std::cout << "*******************NVR SDK  TEST start*********************" << std::endl;
 
	NVRSDKTest test(argv[1], atoi(argv[2]), argv[3], argv[4]);
	test.TestLogin();
	//test.TestGetVersion();

	//test.TestGetCameraList();
	//test.TestGetStatus();
	//test.TestGetUrl();
	//test.TestGetRecord();

   
    //cin >> ch;


//#ifdef LOCALGUI
    test.TestSetLayout();
//#endif

	std::cout << "*******************END NVR SDK  TEST******************" << std::endl;


    char ch;
    cin >> ch;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
