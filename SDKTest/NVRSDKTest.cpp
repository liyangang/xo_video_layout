//#include "pch.h"
#include "NVRSDKTest.h"
#include<iostream>
#include <iomanip>
#include <chrono>
#include <cstring>
#include <vector>
#include"NVRManager.h"

using namespace E40::NVRSDK;

NVRSDKTest::NVRSDKTest()
{
	_deviceIP = "159.99.251.198";
	_deviceUser = "0";
	_devicePassword = "Xo666777";
	_devicePort = 2000;
}
NVRSDKTest::NVRSDKTest(std::string ip, int port, std::string user, std::string pwd)
{
    _deviceIP = ip;
    _devicePort = port;
    _deviceUser = user;
    _devicePassword = pwd;
   
}
NVRSDKTest::~NVRSDKTest()
{
}
void NVRSDKTest::TestLogin()
{
	std::cout << "start test login" << std::endl;

	std::shared_ptr<INVRDevice> nvr = NVRManager::GetInstance().GetNVR(_deviceIP, _devicePort, E40_NVR);
	std::string t_version;
	LoginParam para;
	para.user = _deviceUser;
	para.pwd = "abc";
	para.strIp = _deviceIP;
	para.port = _devicePort;

	
	std::cout << "use wrong password" << std::endl;	
	int ret = nvr->Login(para);

	m_event = new E40::NVRSDK::NVREventHandler(1);
	nvr->AddEventHandler(m_event);

	if (ret == RET_OK)
	{
		std::cout << "login successful" << std::endl;
	}
	else
	{
		std::cout << "login failed" << std::endl;
	}
	
	para.pwd = _devicePassword;
	std::cout << "using right password" << std::endl;
	ret = nvr->Login(para);
	if (ret == RET_OK)
	{
		std::cout << "login successful" << std::endl;
	}
	else
	{
		std::cout << "login failed" << std::endl;
	}
	std::cout << "end test login" << std::endl << std::endl;
}
void NVRSDKTest::TestGetVersion()
{
	std::cout << "start test get version" << std::endl;
	std::shared_ptr<INVRDevice> nvr = NVRManager::GetInstance().GetNVR(_deviceIP, _devicePort, E40_NVR);
	std::string t_version;
	nvr->GetVersion(t_version);
	std::cout << "Version:" << t_version << std::endl;
	std::cout << "end test get version" << std::endl << std::endl;
}
void NVRSDKTest::TestGetCameraList()
{
	std::cout << "start test get camera list" << std::endl;
	std::shared_ptr<INVRDevice> nvr = NVRManager::GetInstance().GetNVR(_deviceIP, _devicePort, E40_NVR);
	std::vector<CamInfo> camList;
	std::vector<int> channelList;
	CamInfo camInfo;
	VideoInfo videoInfo;
	nvr->GetCameralist(camList);
	auto it = camList.begin();
	while (it != camList.end())
	{
		channelList.push_back(it->channel);
		std::cout << it->name << std::endl;
		std::cout << "IP:" << it->ip << std::endl;
		std::cout << "USERNAME:" << it->userName << std::endl;
		std::cout << "STATE:" << it->state << std::endl;
		std::cout << "CHANNEL:" << it->channel << std::endl;
		std::cout << "CAMERADisableD:" << it->camDisabled << std::endl;
		std::cout << "CAMERATYPE:" << it->camType << std::endl;
		if (it->state != 0)
		{
			std::shared_ptr<ICamera> cam = nvr->GetCamera(it->channel);
			cam->GetDeviceInfo(camInfo);
			cam->GetVideoInfo(videoInfo);
			std::cout << camInfo.channel << "," << camInfo.id << std::endl;
			std::cout << "fps:" << videoInfo.fps << std::endl;
			std::cout << "resolution:" << videoInfo.resolution << std::endl;
		}
		/*std::cout << "ASPECTRATIO:" << it->aspectRatio << std::endl;
		std::cout << "DECOTERTYPE:" << it->decoderType << std::endl;
		std::cout << "RESOLUTIONCLASS:" << it->resolutionClass << std::endl;
		std::cout << "PASSWORD:" << it->password << std::endl;
		std::cout << "RTSPPORT:" << it->rtspPort << std::endl;
		std::cout << "RTSPSTREAMTYPE:" << it->rtspStreamtype << std::endl;
		std::cout << "HTTPPORT:" << it->httpPort << std::endl;*/
		it++;
		std::cout << std::endl;
	}
	std::cout << "end test get camera list" << std::endl << std::endl;
}

void NVRSDKTest::TestGetStatus()
{
	std::cout << "start test get camera status" << std::endl;
	std::shared_ptr<INVRDevice> nvr = NVRManager::GetInstance().GetNVR(_deviceIP, _devicePort, E40_NVR);
	std::vector<CamInfo> camList;
	std::vector<int> channelList;
	std::vector<bool> onlineStatus;
	std::vector<bool> recordStatus;
	CamInfo camInfo;
	VideoInfo videoInfo;
	nvr->GetCameralist(camList);
	auto it = camList.begin();
	while (it != camList.end())
	{
		channelList.push_back(it->channel);
		it++;
	}
	nvr->GetCameraStatus(channelList, onlineStatus, CAMERA_ONLINE);
	nvr->GetCameraStatus(channelList, recordStatus, CAMERA_RECORDING);
	for (int i = 0; i < channelList.size(); i++)
	{
		std::cout << "channel" << channelList[i] <<" onlineStatus: "<< onlineStatus[i] << std::endl;
		std::cout << "channel" << channelList[i] <<" recordStatus: "<< recordStatus[i] << std::endl;
	}
	std::cout << "end test get camera status" << std::endl;
}

void NVRSDKTest::TestGetUrl()
{
	std::cout << "start get url" << std::endl;

	std::shared_ptr<INVRDevice> nvr = NVRManager::GetInstance().GetNVR(_deviceIP, _devicePort, E40_NVR);
	int rtspPort = 554;
	int channel = 2; //demo for channel 2
	
	std::string strLiveUrl = nvr->GetLiveUrl(channel, rtspPort);
	std::cout << strLiveUrl.c_str() << std::endl;

	auto totime = std::chrono::system_clock::now();
	time_t to = std::chrono::system_clock::to_time_t(totime);

	std::chrono::duration<int, std::ratio<60 * 60 * 24> > one_day(1);
	auto fromtime = totime - one_day;
	time_t from = std::chrono::system_clock::to_time_t(fromtime);

	std::string strPlaybackUrl = nvr->GetPlaybackUrl(channel, from, to, rtspPort);

	std::cout << strPlaybackUrl.c_str() << std::endl;
}

void NVRSDKTest::TestGetRecord()
{
#define MAX_CAMERAS 32

	std::cout << "start test search video" << std::endl;
	std::shared_ptr<INVRDevice> nvr = NVRManager::GetInstance().GetNVR(_deviceIP, _devicePort, E40_NVR);
	nvr->SetPlaybackCallback(NVRSDKTest::PlaybackCallback);
	std::vector<int> channelList;
	channelList.push_back(1);
	channelList.push_back(2); // //demo for search 1,2 channel

	auto totime = std::chrono::system_clock::now();
	time_t to = std::chrono::system_clock::to_time_t(totime);

	std::chrono::duration<int, std::ratio<60 * 60 * 24> > one_day(1);
	auto fromtime = totime - one_day;
	time_t from = std::chrono::system_clock::to_time_t(fromtime);

	std::string eventType;
	nvr->SearchVideo(channelList, from, to, eventType);

    std::cout << "end test search video" << std::endl;
}

void NVRSDKTest::PlaybackCallback(DeviceRecordInfo pRecordInfo)
{
	if(pRecordInfo.recordType == DEVICE_HISTORY_SEARCH_ERROR)
		std::cout << "search error" << std::endl;
	else if(pRecordInfo.recordType == DEVICE_HISTORY_SEARCH_FINISH)
		std::cout << "search finish" << std::endl;
	else if (pRecordInfo.recordType == DEVICE_HISTORY_SEARCH_RECROD)
	{
#if defined(__WIN32__) || defined(_WIN32)
		std::tm tmStart = {};
		std::tm tmEnd = {};
		localtime_s(&tmStart, &pRecordInfo.tmStart);
		localtime_s(&tmEnd, &pRecordInfo.tmEnd);
#else
		std::tm tmStart = *std::localtime(&pRecordInfo.tmStart);
		std::tm tmEnd = *std::localtime(&pRecordInfo.tmEnd);
#endif
		auto start = std::put_time(&tmStart, "%F %H:%M:%S");

		auto end = std::put_time(&tmEnd, "%F %H:%M:%S");
		std::cout << pRecordInfo.ucCamera << " "<< pRecordInfo.ui64RecId << " "<<start << " "<< end << " " << pRecordInfo.eventType.c_str()<<std::endl;
	}
}


void NVRSDKTest::TestSetLayout()
{
    std::cout << "******************************************" << std::endl;
    std::cout << "start test SetLayout Interface" << std::endl;
 
    VideoLayout layout[1];
    layout[0].monitor = 0;
    layout[0].width   = 1920;
    layout[0].height  = 1080;

    VideoPanel panels[6];
    
    panels[0].osd = true;
    panels[0].stream_channel = 0;
    panels[0].x = 1;
    panels[0].y = 1;
    panels[0].w = 1279;
    panels[0].h = 719;

    panels[1].osd = false;
    panels[1].stream_channel = 1;
    panels[1].x = 1281;
    panels[1].y = 1;
    panels[1].w = 638;
    panels[1].h = 358;

    panels[2].osd = false;
    panels[2].stream_channel = -1;
    panels[2].x = 1281;
    panels[2].y = 360;
    panels[2].w = 638;
    panels[2].h = 358;

    panels[3].osd = false;
    panels[3].stream_channel = -1;
    panels[3].x = 1;
    panels[3].y = 721;
    panels[3].w = 638;
    panels[3].h = 358;

    panels[4].osd = false;
    panels[4].stream_channel = -1;
    panels[4].x = 640;
    panels[4].y = 721;
    panels[4].w = 638;
    panels[4].h = 358;


    panels[5].osd = false;
    panels[5].stream_channel = -1;
    panels[5].x = 1281;
    panels[5].y = 721;
    panels[5].w = 638;
    panels[5].h = 358;
    
    for (int i = 0; i < 6; i++)
    {
        layout[0].m_panellist.push_back(panels[i]);
    }
    
    std::vector<VideoLayout> list_layout;
    list_layout.push_back(layout[0]);
    /*
    layout[1].monitor = 1;
    layout[1].width = 1920;
    layout[1].height = 1080;

    VideoPanel panel_1;
    panel_1.osd = false;
    panel_1.stream_channel = 7;
    panel_1.x = 1;
    panel_1.y = 1;
    panel_1.w = 1918;
    panel_1.h = 1078;
    layout[1].m_panellist.push_back(panel_1);
    list_layout.push_back(layout[1]);
    */
    std::shared_ptr<INVRDevice> nvr = NVRManager::GetInstance().GetNVR(_deviceIP, _devicePort, E40_NVR);
    
    nvr->SetLayout(list_layout);
}

