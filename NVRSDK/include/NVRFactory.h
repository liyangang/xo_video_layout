#ifndef NVRSDK_NVRFACTORY_H
#define NVRSDK_NVRFACTORY_H
#include<memory>
#include"INVRDevice.h"
#include<map>
namespace E40 {
	namespace NVRSDK {

		class NVRFactory
		{
		public:
			static NVRFactory& GetInstance();
			std::shared_ptr<INVRDevice> GetNVR(const std::string& t_nvrName, const NVRTYPE t_type = E40_NVR);
		public:
			~NVRFactory();

			NVRFactory(const NVRFactory&) = delete;
			NVRFactory(NVRFactory&&) = delete;
			NVRFactory& operator=(const NVRFactory&) = delete;
			NVRFactory& operator=(NVRFactory&&) = delete;
		private:
			NVRFactory();
			std::map<std::string, std::shared_ptr<INVRDevice>> m_mapNVRs;
		};
	}
}

#endif //NVRSDK_NVRFACTORY_H

