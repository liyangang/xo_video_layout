#ifndef NVRSDK_NVRPLAYBACK_H
#define NVRSDK_NVRPLAYBACK_H
#include<string>
namespace E40 {
	namespace NVRSDK {
		enum DEVICE_EN_SEARCH_ACTION
		{
			/// <summary>
			/// Begin video search.
			/// </summary>
			HISTORY_SEARCH_BEGIN = 0,
			
			/// <summary>
			/// Finish video search.
			/// </summary>
			DEVICE_HISTORY_SEARCH_FINISH = 1,

			/// <summary>
			/// Find a valid record.
			/// </summary>
			DEVICE_HISTORY_SEARCH_RECROD = 2,

			/// <summary>
			/// Search action failed.
			/// </summary>
			DEVICE_HISTORY_SEARCH_ERROR = 3
		};


		typedef struct RecordInfo
		{
			std::uint32_t ucCamera;

			/// <summary>
			/// RecordType, indicate this record type.
			/// </summary>
			DEVICE_EN_SEARCH_ACTION recordType;

			/// <summary>
			/// Start, start time of this video record.
			/// </summary>
			time_t tmStart;

			/// <summary>
			/// End, end time of this video record.
			/// </summary>
			time_t tmEnd;

			/// <summary>
			/// Additional video data used to let channel playback this record.
			/// </summary>
			std::uint64_t ui64RecId;

			//if eventType == "", schedule record, if eventType == "1", motion record
			std::string eventType;

		} DeviceRecordInfo;

#if defined(__WIN32__) || defined(_WIN32)
		typedef void(__stdcall *PlaybackCallback)(DeviceRecordInfo pRecrodInfo);
#else
		typedef void(*PlaybackCallback)(DeviceRecordInfo pRecrodInfo);
#endif

	}
}
#endif //NVRSDK_NVRPLAYBACK_H
