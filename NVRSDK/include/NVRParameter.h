#ifndef NVRSDK_NVRPARAMETER_H
#define NVRSDK_NVRPARAMETER_H
#include<string>
namespace E40 {
	namespace NVRSDK {
		enum NVRTYPE {
			E40_NVR=0
		};

		enum EVENTTYPE {
			CAMERA_ONLINE						= 0,	//Camera event type nbr from 0 to 99;
			CAMERA_RECORDING					= 1,
			CAMERA_MOTION						= 2,
			TECHNICIAN_GRANT					= 100   //NVR event type nbr from 100 to 500;
		};

		struct LoginParam {
			std::string user;			//user
			std::string pwd;			//password
			std::string strIp;			//NVR IP
			unsigned short port;		//NVR connecting port
		};
		struct CamInfo {
			std::string  ip;			//camera IP
			std::string  name;			//camera name
			std::string  id;			//camera ID
			std::string	 userName;		//camera http login username
			std::string  camType;		//camera type
			int			 state;			//camera state
			int          channel;		//channel of camera binded 
			int			 camDisabled;	//0 means camera is avaliable, 1 means the camera is not avaliable
		};

		struct VideoInfo {
			std::string  rtspPort;			//camera rtsp port
			std::string  httpPort;			//camera http port
			std::string  aspectRatio;		//camera video aspect 
			std::string  rtspStreamtype;	//rtsp streamt type
			std::string  resolutionClass;	//resolution class
			std::string  resolution;		//video resolution
			int  fps;						//video fps
			int  constrast;					//video constrast
			int  birghtness;				//video brightness
			int  saturation;				//video saturation
		};

		struct CameraDetail {
			CamInfo		camConfig;
			VideoInfo	videoConfig;
		};

		struct NVREventMsg {
			int				channel;				//channel id
			EVENTTYPE		eventId;				//event id
			bool			status;					//event status
			std::string		extraData;				//extra data for certain alarm event
		};


        class VideoPanel
        {
        public:
            //stream channel binded to the videopanel,
            //if no stream binded, the value is -1
            int stream_channel = -1; 
            bool osd = false;
            int  x = 0;     // video panel start x screen position, 
            int  y = 0;     // video panel start y screen position, 
            int  w = 0;     // video panel width 
            int  h = 0;     // video panel height
        };

        class VideoLayout
        {
        public:
            int monitor = 0; // monitor index ,default is 0
            int width   = 0; //whole video area width
            int height  = 0; //whole video area height

            std::vector<VideoPanel> m_panellist; //video panel list

        };


	}
}
#endif //NVRSDK_NVRPARAMETER_H
