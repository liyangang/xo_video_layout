#ifndef NVRSDK_INVRDEVICE_H
#define NVRSDK_INVRDEVICE_H
#include <vector>
#include <map>
#include"NVRParameter.h"
#include"sdkError.h"
#include "NVRPlayback.h"
#include<memory>
namespace E40 {
	namespace NVRSDK {

		class ICamera {
		public:
			virtual ~ICamera() {};
			virtual void GetDeviceInfo(CamInfo& CamInfo)										  = 0;
			virtual void GetVideoInfo(VideoInfo& Video)											  = 0;
		
		};
		class INVREventHandler {
		public:
			virtual ~INVREventHandler() {};
			virtual void HandleEvent(NVREventMsg eventMsg)										  = 0;
		};

		class INVRDevice {
		public:
			virtual  ~INVRDevice() {};
			/*
			@t_loginPara[IN]: parameters need to login NVR
			@return value:   RET_OK, successfully
			*/
			virtual int Login(const LoginParam& t_loginPara)                                      = 0;

			/*
			@t_vesion[OUT]: version get from sdk
			@return value:   RET_OK, successfully
			*/
			virtual int GetVersion(std::string& t_vesion)                                         = 0;

			/*
			@t_cameraList[OUT]: camera list get from sdk
			@return value:   RET_OK, successfully
			*/
			virtual int GetCameralist(std::vector<CamInfo>& t_cameraList)						  = 0;
			virtual std::shared_ptr<ICamera> GetCamera(int channel)                               = 0;

			virtual void SetPlaybackCallback(PlaybackCallback fptrCallback)					      = 0;

			//if eventType == "", search all video, if eventType == "1", search motion video
			virtual bool SearchVideo(const std::vector<int>& channelList, time_t from, time_t to, 
                const std::string& eventType) = 0;
			virtual void SearchStop() = 0;

			virtual std::string GetLiveUrl(int channel, int rtspPort) = 0;
			virtual std::string GetPlaybackUrl(int channel, time_t from, time_t to, int rtspPort) = 0;

			/*
			before calling GetCameraStatus function, camera list should be obtained using GetCameralist function
			@t_channel[IN]: list of cameras' channel num
			@staType[IN]: status typy: ONLINE, RECORDING
			@t_status>[OUT]: list of camera status, the index correspond to the "t-channel[IN]"
			*/
			virtual int GetCameraStatus(const std::vector<int>& t_channels, 
				std::vector<bool>& t_status, EVENTTYPE staType)								  = 0;
			/*
			@t_handler[IN] a pointer of INVREventHandler
			*/
			virtual void AddEventHandler(INVREventHandler* t_handler)							  = 0;
			/*
			@t_handler[IN] a pointer of INVREventHandler
			*/
			virtual void RemoveEventHandler(INVREventHandler* t_handler)		                  = 0;
//#ifdef LOCALGUI
// Interfaces here aer only used by Local GUI, for the Local GUI program, it need define MACRO "LOCALGUI"
            
            /*
            @t_layout[IN]: List of VideoLayout, if there are more than monitors and each monitor display
            a separate video video, then it need push each monitor's VideoLayout into the list   
            @return value:   RET_OK, successfully
            */
            virtual int SetLayout(std::vector<VideoLayout>& t_layout) = 0;

//#endif 

		};
	}

}
#endif //NVRSDK_INVRDEVICE_H
