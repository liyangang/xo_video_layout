#ifndef NVRSDK_NVRMANAGER_H
#define NVRSDK_NVRMANAGER_H

#ifdef _USRDLL

#ifdef NVRSDK_EXPORTS
#define DLLEXPORT __declspec(dllexport)
#else
#define DLLEXPORT __declspec(dllimport)
#endif

#else
#define DLLEXPORT
#endif

#include<memory>
#include"INVRDevice.h"


namespace E40 {
	namespace NVRSDK {
		class DLLEXPORT NVRManager {
		public:
			/*
			 @return value: NVRManager singleton
			*/
			static NVRManager& GetInstance();
			/*
			 @t_IP[IN]: NVR IP address
			 @t_port[IN]: NVR connecting port
			 @t_type[IN]:    NVR types defined in nvrparameter.h
			 @return value: shared pointer of INVRDevice, no need to delete
			*/
			std::shared_ptr<INVRDevice> GetNVR(const std::string& t_IP, const unsigned short t_port,
				const NVRTYPE t_type = E40_NVR);
		public:
			~NVRManager();

			NVRManager(const NVRManager&) = delete;
			NVRManager(NVRManager&&) = delete;
			NVRManager& operator=(const NVRManager&) = delete;
			NVRManager& operator=(NVRManager&&) = delete;
		private:
			NVRManager();
		};
	}
}
#endif //NVRSDK_NVRMANAGER_H
	