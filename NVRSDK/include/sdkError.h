#ifndef NVRSDK_SDKERROR_H
#define NVRSDK_SDKERROR_H
//device 0-100
#define RET_OK                             0
#define ERR_LOGIN_AUTHENTICATION		   1
#define ERR_LOGIN_INIT                     2
#define ERR_GET_VERSION					   3
#define ERR_ENCRYPT						   4
#define ERR_GETZIPPED					   5
#define ERR_SETLAYOUT                      6
#define ERROR_NOT_IN_USE				   7
#define ERROR_NO_CONTROL_CONNECTION		   8
#endif //NVRSDK_SDKERROR_H
